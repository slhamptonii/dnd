package main

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

var er = errors.New("test error")

func TestHandleRequest(t *testing.T) {
	headers := map[string]string{"alfa": "bravo"}

	//405
	res, err := HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
		Headers:    headers,
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusMethodNotAllowed, res.StatusCode)
	assert.Equal(t, "nah, bro", res.Body)

	//Fail - No Token
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "DELETE",
		Headers:    headers,
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusForbidden, res.StatusCode)
	assert.Equal(t, "invalid sender", res.Body)

	//Delete Fail
	dynamoSvc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "DELETE",
		Headers:    headers,
		Path:       "/users/someone/items/chicken",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusInternalServerError, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "could not delete item", res.Body)

	//Delete Success
	dynamoSvc = &mockOutItem{}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "DELETE",
		Headers:    headers,
		Path:       "/users/someone/items/chicken",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusNoContent, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "", res.Body)

	//Create Fail - bad json
	dynamoSvc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		Headers:    headers,
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/someone/items/chicken",
		Body: "!@#$%^&*()",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "invalid payload", res.Body)

	//Create Fail - bad dynamo put
	dynamoSvc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		Headers:    headers,
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/someone/items/chicken",
		Body: "{\"name\": \"chicken\", \"owner\":\"someone\"}",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusInternalServerError, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "could not save item", res.Body)

	//Create Success
	dynamoSvc = &mockOutItem{}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		Headers:    headers,
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/someone",
		Body: "{\"name\": \"chicken\", \"owner\":\"someone\"}",
	})
	assert.Equal(t, http.StatusCreated, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.NotEmpty(t, res.Body)
	assert.Nil(t, err)
}

func TestDeleteItem(t *testing.T) {
	//Success
	dynamoSvc = &mockOutItem{}
	ok := DeleteItem("id", "somebody")
	assert.Equal(t, true, ok)

	//Fail
	dynamoSvc = &mockOutItem{Err: er}
	ok = DeleteItem("id", "nobody")
	assert.Equal(t, false, ok)
}

func TestNewId(t *testing.T) {
	assert.NotEmpty(t, newId())
}

type mockOutItem struct {
	dynamodbiface.DynamoDBAPI
	Err error
}

func (m *mockOutItem) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	return &dynamodb.PutItemOutput{}, m.Err
}

func (m mockOutItem) DeleteItem(input *dynamodb.DeleteItemInput) (*dynamodb.DeleteItemOutput, error) {
	return &dynamodb.DeleteItemOutput{}, m.Err
}
