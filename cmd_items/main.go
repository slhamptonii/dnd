package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	lam "github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/aws/aws-sdk-go/service/lambda/lambdaiface"
	"github.com/google/uuid"
	request "gitlab.com/slhamptonii/sheldonsandbox-core/req"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
	"log"
	"net/http"
	"os"
)

var tableName = os.Getenv("ITEM_TABLE_NAME")
var dynamoSvc dynamodbiface.DynamoDBAPI
var lambdaSvc lambdaiface.LambdaAPI

func init() {
	dynamoSvc = sdk.DynamoConnect()
	lambdaSvc = sdk.LambdaConnect()
}

func HandleRequest(r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	ss := sdk.PullFromCtx(&r.RequestContext, "ss")[0]
	signedString, ok := ss.(string)
	if !ok {
		return sdk.NoAuthRes(http.StatusForbidden, "invalid sender")
	}
	headers := map[string]string{"x-ss": signedString}
	headers["content-type"] = "application/json"
	headers["access-control-allow-origin"] = "*"
	headers["access-control-expose-headers"] = "x-ss"
	headers["accept"] = "*/*"

	switch r.HTTPMethod {
	case "POST":
		var item model.Item
		err := json.Unmarshal([]byte(r.Body), &item)
		if err != nil {
			log.Println(r.RequestContext.RequestID, "could not unmarshal payload", err)
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		item.Id = newId()
		ok := sdk.Put(dynamoSvc, tableName, item)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "could not save item")
		}

		b, err := json.Marshal(item)
		if err != nil {
			log.Print("could not marshal item", err)
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		return sdk.ShortRes(http.StatusCreated, headers, string(b))
	case "DELETE":
		vars := request.ExtractPath(r.Path)
		username, id := vars[1], vars[3]
		ok := DeleteItem(id, username)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "could not delete item")
		}
		return sdk.ShortRes(http.StatusNoContent, headers, "")
	case "PATCH":
		vars := request.ExtractPath(r.Path)
		username, id := vars[1], vars[3]

		var newItem model.Item
		err := json.Unmarshal([]byte(r.Body), &newItem)
		if err != nil {
			log.Println(r.RequestContext.RequestID, "could not unmarshal payload", err)
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		if username == "" || id == "" || id != newItem.Id {
			log.Println(r.RequestContext.RequestID, "bad username or id", username, id)
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		expressionAttributeNames := map[string]*string{}
		expressionAttributeValues := map[string]*dynamodb.AttributeValue{}
		key := map[string]*dynamodb.AttributeValue{}
		key["id"] = &dynamodb.AttributeValue{S: aws.String(newItem.Id)}
		key["owner"] = &dynamodb.AttributeValue{S: aws.String(newItem.Owner)}

		nameExpression := "#NAME_EXPRESSION = :nameExpression"
		expressionAttributeNames["#NAME_EXPRESSION"] = aws.String("name")
		expressionAttributeValues[":nameExpression"] = &dynamodb.AttributeValue{S: aws.String(newItem.Name)}

		descriptionExpression := "#DESCRIPTION_EXPRESSION = :descriptionExpression"
		expressionAttributeNames["#DESCRIPTION_EXPRESSION"] = aws.String("description")
		expressionAttributeValues[":descriptionExpression"] = &dynamodb.AttributeValue{S: aws.String(newItem.Description)}

		updateInput := &dynamodb.UpdateItemInput{
			ExpressionAttributeNames:  expressionAttributeNames,
			ExpressionAttributeValues: expressionAttributeValues,
			Key:                       key,
			ReturnValues:              aws.String("ALL_NEW"),
			TableName:                 aws.String(tableName),
			UpdateExpression:          aws.String(fmt.Sprintf("SET %s, %s", nameExpression, descriptionExpression)),
		}

		_, ok = sdk.UpdateItem(dynamoSvc, updateInput)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "unable to process item update")
		}

		b, err := json.Marshal(newItem)
		if err != nil {
			log.Println("could not marshal item", err)
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		return sdk.ShortRes(http.StatusOK, headers, string(b))
	case "PUT":
		vars := request.ExtractPath(r.Path)
		username, id := vars[1], vars[3]

		var newItem model.Item
		err := json.Unmarshal([]byte(r.Body), &newItem)
		if err != nil {
			log.Println(r.RequestContext.RequestID, "could not unmarshal payload", err)
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		if username != newItem.Owner || id == "" || id != newItem.Id {
			log.Println(r.RequestContext.RequestID, "bad username or id", username, id)
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		_, ok := findUser(newItem.Owner, signedString)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "unable to process item update")
		}

		expressionAttributeNames := map[string]*string{}
		expressionAttributeValues := map[string]*dynamodb.AttributeValue{}
		key := map[string]*dynamodb.AttributeValue{}
		key["id"] = &dynamodb.AttributeValue{S: aws.String(newItem.Id)}
		key["owner"] = &dynamodb.AttributeValue{S: aws.String(newItem.Owner)}

		ownerExpression := "#OWNER_EXPRESSION = :ownerExpression"
		expressionAttributeNames["#OWNER_EXPRESSION"] = aws.String("owner")
		expressionAttributeValues[":ownerExpression"] = &dynamodb.AttributeValue{S: aws.String(newItem.Owner)}

		updateInput := &dynamodb.UpdateItemInput{
			ExpressionAttributeNames:  expressionAttributeNames,
			ExpressionAttributeValues: expressionAttributeValues,
			Key:                       key,
			ReturnValues:              aws.String("ALL_NEW"),
			TableName:                 aws.String(tableName),
			UpdateExpression:          aws.String(fmt.Sprintf("SET %s", ownerExpression)),
		}

		_, ok = sdk.UpdateItem(dynamoSvc, updateInput)
		if !ok {
			log.Println("could not decode response")
			return sdk.ShortRes(http.StatusInternalServerError, headers, "unable to process user update")
		}

		b, err := json.Marshal(newItem)
		if err != nil {
			log.Println("could not marshal user", err)
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		return sdk.ShortRes(http.StatusOK, headers, string(b))
	default:
		return sdk.NoAuthRes(http.StatusMethodNotAllowed, "nah, bro")
	}
}

func findUser(username, signedString string) (model.User, bool) {
	var user model.User
	log.Println("searching for", username)
	path := fmt.Sprintf("/users/%s", username)
	req := events.APIGatewayProxyRequest{
		Path:       path,
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{
			Identity:   events.APIGatewayRequestIdentity{},
			Authorizer: map[string]interface{}{"ss": signedString},
		},
	}

	payload, err := json.Marshal(req)
	if err != nil {
		log.Println("could not marshal lambda request ", err)
		return model.User{}, false
	}

	invokeInput := &lambda.InvokeInput{
		FunctionName: aws.String("sheldonsandbox-qry-users"),
		Payload:      payload,
	}

	if output, ok := sdk.Invoke(lambdaSvc, invokeInput); !ok {
		log.Println("function invoke failed ", *output.FunctionError)
		return model.User{}, false
	} else {
		var res events.APIGatewayProxyResponse
		r := bytes.NewReader(output.Payload)
		err = json.NewDecoder(r).Decode(&res)
		if err != nil {
			log.Println("could not decode response", err)
			return model.User{}, false
		}

		err := json.Unmarshal([]byte(res.Body), &user)
		if err != nil {
			log.Println("could not unmarshal body into user", res.Body, err)
			return model.User{}, false
		}
	}
	return user, true
}

func DeleteItem(id, owner string) bool {
	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
			"owner": {
				S: aws.String(owner),
			},
		},
		TableName: aws.String(tableName),
	}

	return sdk.DeleteItem(dynamoSvc, input)
}

func newId() string {
	return uuid.New().String()
}

func main() {
	lam.Start(HandleRequest)
}
