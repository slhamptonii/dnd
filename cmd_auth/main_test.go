package main

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"net/http"
	"testing"
)

var er = errors.New("test error")

func TestHandleRequest(t *testing.T) {
	headers := map[string]string{"alfa": "bravo"}

	//Fail - 405
	res, err := HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
		Headers:    headers,
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusMethodNotAllowed, res.StatusCode)
	assert.Equal(t, "nah, bro", res.Body)

	//Fail - No Token
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
		Headers:    headers,
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusForbidden, res.StatusCode)
	assert.Equal(t, "invalid sender", res.Body)

	//Fail - bad payload
	svc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		Headers:    headers,
		Body:       "{\"userna\n}",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
	assert.Equal(t, "invalid payload", res.Body)

	//Fail - could not find user
	svc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		Headers:    headers,
		Body:       "{\"username\":\"Testie\",\"password\":\"drowssap\"}",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusForbidden, res.StatusCode)
	assert.Equal(t, "nope", res.Body)

	//Fail - bad username
	svc = &mockOutItem{}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		Headers:    headers,
		Body:       "{\"username\":\"Tester\",\"password\":\"drowssap\"}",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusForbidden, res.StatusCode)
	assert.Equal(t, "nope", res.Body)

	//Fail - bad password
	svc = &mockOutItem{}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		Headers:    headers,
		Body:       "{\"username\":\"Testie\",\"password\":\"nugget\"}",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusForbidden, res.StatusCode)
	assert.Equal(t, "nope", res.Body)

	//Success
	svc = &mockOutItem{Item: map[string]*dynamodb.AttributeValue{
		"Username": {S: aws.String("Testie")},
		"Password": {S: aws.String("$2a$04$TqXBBTTnZbjpOND/HkTWyOmFvFxqOuXSrafSWiGj7V4RN3qdDxo3O")},
		"Role":     {N: aws.String("1")},
		"Party":    {S: aws.String("bus")},
	},
		Err: nil,
	}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		Headers:    headers,
		Body:       "{\"username\":\"Testie\",\"password\":\"drowssap\"}",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "{\"username\":\"Testie\",\"password\":\"\",\"role\":1,\"party\":\"bus\"}", res.Body)
	assert.Nil(t, err)
}

func TestGetUser(t *testing.T) {
	//Success
	svc = &mockOutItem{Item: map[string]*dynamodb.AttributeValue{
		"Username": {S: aws.String("Testie")},
		"Password": {S: aws.String("drowssap")},
		"Role":     {N: aws.String("1")},
		"Party":    {S: aws.String("bus")},
	},
		Err: nil,
	}
	user := GetUser("Testie")
	assert.Equal(t, "Testie", user.Username)
	assert.Equal(t, "drowssap", user.Password)
	assert.Equal(t, model.Player, user.Role)
	assert.Equal(t, "bus", user.Party)

	//Fail - could not find user
	svc = &mockOutItem{Err: er}
	user = GetUser("nobody")
	assert.Equal(t, "", user.Username)
	assert.Equal(t, "", user.Password)
	assert.Equal(t, model.Stranger, user.Role)
	assert.Equal(t, "", user.Party)
}

type mockOutItem struct {
	dynamodbiface.DynamoDBAPI
	Item map[string]*dynamodb.AttributeValue
	Err  error
}

func (m mockOutItem) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {
	return &dynamodb.GetItemOutput{Item: m.Item}, m.Err
}
