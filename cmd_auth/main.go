package main

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"os"
)

var tableName = os.Getenv("TABLE_NAME")
var svc dynamodbiface.DynamoDBAPI

func init() {
	svc = sdk.DynamoConnect()
}

func HandleRequest(r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	ss := sdk.PullFromCtx(&r.RequestContext, "ss")[0]
	signedString, ok := ss.(string)
	if !ok {
		return sdk.NoAuthRes(http.StatusForbidden, "invalid sender")
	}
	headers := map[string]string{"x-ss": signedString}
	headers["content-type"] = "application/json"
	headers["access-control-allow-origin"] = "*"
	headers["access-control-expose-headers"] = "x-ss"
	headers["accept"] = "*/*"

	if r.HTTPMethod == "POST" {
		var stranger model.User
		err := json.Unmarshal([]byte(r.Body), &stranger)
		if err != nil {
			log.Println(r.RequestContext.RequestID, "could not unmarshal payload", err)
			return sdk.NoAuthRes(http.StatusBadRequest, "invalid payload")
		}

		user := GetUser(stranger.Username)

		if !comparePasswords(user.Password, []byte(stranger.Password)) {
			return sdk.NoAuthRes(http.StatusForbidden, "nope")
		}

		user.Password = ""
		b, err := json.Marshal(user)
		if err != nil {
			log.Println("could not marshal user", err)
			return sdk.NoAuthRes(http.StatusInternalServerError, "we have failed you")
		}

		return sdk.ShortRes(http.StatusOK, headers, string(b))
	} else {
		return sdk.NoAuthRes(http.StatusMethodNotAllowed, "nah, bro")
	}
}

func GetUser(username string) model.User {
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"username": {
				S: aws.String(username),
			},
			"role": {
				N: aws.String("1"),
			},
		},
	})
	if err != nil {
		log.Println("error calling GetItem", err)
		return model.User{}
	}

	user := model.User{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &user)
	if err != nil {
		log.Println("could not unmarshal user", err)
		return model.User{}
	}

	return user
}

func comparePasswords(hashedPwd string, plainPwd []byte) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		log.Println("could not verify password: ", err)
		return false
	}

	return true
}

func main() {
	lambda.Start(HandleRequest)
}
