package main

import (
	"gitlab.com/slhamptonii/dnd_sender/io"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
	"log"
	"net/http"
	"time"
)

func main() {
	cBridge, wBridge := io.Bridge{}, io.Bridge{}

	uCToW, iCToW, pCToW :=
		make(chan io.Instruction), make(chan io.Instruction), make(chan io.Instruction)
	uWToC, iWToC, pWToC :=
		make(chan io.Instruction), make(chan io.Instruction), make(chan io.Instruction)

	//Read me carefully
	cBridge.OutUser, wBridge.InUser = uCToW, uCToW
	cBridge.OutItem, wBridge.InItem = iCToW, iCToW
	cBridge.OutParty, wBridge.InParty = pCToW, pCToW
	wBridge.OutUser, cBridge.InUser = uWToC, uWToC
	wBridge.OutItem, cBridge.InItem = iWToC, iWToC
	wBridge.OutParty, cBridge.InParty = pWToC, pWToC

	w := io.Writer{
		Bridge: &wBridge,
		Svc:    sdk.DynamoConnect(),
	}

	c := io.Consumer{
		Bridge: &cBridge,
		Svc:    sdk.SQSConnect(),
	}

	log.Print("connecting writer")
	log.Print("connecting consumer")
	if !w.Connect() || !c.Connect() {
		log.Fatalln("could not connect to services", c.UserUrl, c.ItemUrl, c.PartyUrl)
		return
	}

	log.Println("starting writer process")
	go w.Process()

	log.Println("starting consumer process")
	go c.Process()

	time.After(5 * time.Second)
	log.Println("consuming")
	go c.Consume()

	//Healthcheck
	//go func() {
	http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "HEAD" {
			log.Println("healthy")
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusMethodNotAllowed)
			_, _ = w.Write([]byte("method not allowed"))
		}
	})
	_ = http.ListenAndServe(":80", nil)
	//}()

	defer recoverError(&c, &w)
}

func recoverError(c *io.Consumer, w *io.Writer) {
	if r := recover(); r != nil {
		log.Println("the end has come")
		w.CleanUp()
		c.CleanUp()
		log.Println("go in peace")
	}
}
