package io

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
	"log"
	"strconv"
	"sync"
)

type Option int8

const (
	Put Option = iota
	Remove
	Modify
)

type Instruction struct {
	Status int8 //-1: fail, 0:unset, 1:success
	Id     string
	Op     Option
	It     interface{}
	User   model.User
	Item   model.Item
	Party  model.Party
}

type Writer struct {
	Bridge *Bridge
	Svc    dynamodbiface.DynamoDBAPI
}

func (w *Writer) Connect() bool {
	w.Svc = dynamodbConnect()
	return true
}

func (w *Writer) Process() {
	for {
		select {
		case inst := <-w.Bridge.InUser:
			w.processUser(inst)
		case inst := <-w.Bridge.InItem:
			w.processItem(inst)
		case inst := <-w.Bridge.InParty:
			w.processParty(inst)
		}
	}
}

func (w *Writer) processParty(inst Instruction) {
	if w.handleParty(inst) {
		inst.Status += 1
	} else {
		inst.Status -= 1
	}
	w.Bridge.OutParty <- inst
}

func (w *Writer) processItem(inst Instruction) {
	if w.handleItem(inst) {
		inst.Status += 1
	} else {
		inst.Status -= 1
	}
	w.Bridge.OutItem <- inst
}

func (w *Writer) processUser(inst Instruction) {
	if w.handleUser(inst) {
		log.Println("putting user went well")
		inst.Status += 1
	} else {
		log.Println("putting user failed")
		inst.Status -= 1
	}
	w.Bridge.OutUser <- inst
}

func (w *Writer) handleUser(inst Instruction) bool {
	switch inst.Op {
	case Put:
		return sdk.Put(w.Svc, getUserTable(), inst.User)
	case Remove:
		input := &dynamodb.DeleteItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"username": {
					S: aws.String(inst.User.Username),
				},
				"role": {
					N: aws.String(strconv.Itoa(int(inst.User.Role))),
				},
			},
			TableName: aws.String(getUserTable()),
		}

		return sdk.DeleteItem(w.Svc, input)
	case Modify:
		log.Println("user to modify", inst.User)
		updateInput := makeUpdateUserInput(inst)
		_, ok := sdk.UpdateItem(w.Svc, updateInput)
		return ok
	default:
		log.Println("invalid operation", inst)
		return false
	}
}

func makeUpdateUserInput(inst Instruction) *dynamodb.UpdateItemInput {
	field, value := "", ""

	if inst.User.Password == "" {
		field, value = "party", inst.User.Party
	} else {
		field, value = "password", inst.User.Password
	}

	expressionAttributeNames := map[string]*string{}
	expressionAttributeValues := map[string]*dynamodb.AttributeValue{}
	key := map[string]*dynamodb.AttributeValue{}
	key["username"] = &dynamodb.AttributeValue{S: aws.String(inst.User.Username)}
	key["role"] = &dynamodb.AttributeValue{N: aws.String(strconv.Itoa(int(inst.User.Role)))}

	partyExpression := "#EXPRESSION = :expression"
	expressionAttributeNames["#EXPRESSION"] = aws.String(field)
	expressionAttributeValues[":expression"] = &dynamodb.AttributeValue{S: aws.String(value)}

	updateInput := &dynamodb.UpdateItemInput{
		ExpressionAttributeNames:  expressionAttributeNames,
		ExpressionAttributeValues: expressionAttributeValues,
		Key:                       key,
		ReturnValues:              aws.String("ALL_NEW"),
		TableName:                 aws.String(getUserTable()),
		UpdateExpression:          aws.String(fmt.Sprintf("SET %s", partyExpression)),
	}
	return updateInput
}

func (w *Writer) handleItem(inst Instruction) bool {
	switch inst.Op {
	case Put:
		return sdk.Put(w.Svc, getItemTable(), inst.Item)
	case Remove:
		input := &dynamodb.DeleteItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"id": {
					S: aws.String(inst.Item.Id),
				},
				"owner": {
					S: aws.String(inst.Item.Owner),
				},
			},
			TableName: aws.String(getItemTable()),
		}

		return sdk.DeleteItem(w.Svc, input)
	case Modify:
		log.Println("item to modify", inst.Item)
		updateInput := makeUpdateItemInput(inst)
		_, ok := sdk.UpdateItem(w.Svc, updateInput)
		return ok
	default:
		log.Println("invalid operation", inst)
		return false
	}
}

func makeUpdateItemInput(inst Instruction) *dynamodb.UpdateItemInput {
	soft := inst.Item.Name != ""
	expressionAttributeNames := map[string]*string{}
	expressionAttributeValues := map[string]*dynamodb.AttributeValue{}
	key := map[string]*dynamodb.AttributeValue{}
	key["id"] = &dynamodb.AttributeValue{S: aws.String(inst.Item.Id)}
	key["owner"] = &dynamodb.AttributeValue{S: aws.String(inst.Item.Owner)}

	var updateExpression string

	if soft {
		nameExpression := "#NAME_EXPRESSION = :nameExpression"
		expressionAttributeNames["#NAME_EXPRESSION"] = aws.String("name")
		expressionAttributeValues[":nameExpression"] = &dynamodb.AttributeValue{S: aws.String(inst.Item.Name)}

		descriptionExpression := "#DESCRIPTION_EXPRESSION = :descriptionExpression"
		expressionAttributeNames["#DESCRIPTION_EXPRESSION"] = aws.String("description")
		expressionAttributeValues[":descriptionExpression"] = &dynamodb.AttributeValue{S: aws.String(inst.Item.Description)}

		updateExpression = fmt.Sprintf("%s, %s", nameExpression, descriptionExpression)
	} else {
		ownerExpression := "#OWNER_EXPRESSION = :ownerExpression"
		expressionAttributeNames["#OWNER_EXPRESSION"] = aws.String("owner")
		expressionAttributeValues[":ownerExpression"] = &dynamodb.AttributeValue{S: aws.String(inst.Item.Owner)}
		updateExpression = ownerExpression
	}

	updateInput := &dynamodb.UpdateItemInput{
		ExpressionAttributeNames:  expressionAttributeNames,
		ExpressionAttributeValues: expressionAttributeValues,
		Key:                       key,
		ReturnValues:              aws.String("ALL_NEW"),
		TableName:                 aws.String(getItemTable()),
		UpdateExpression:          aws.String(fmt.Sprintf("SET %s", updateExpression)),
	}
	return updateInput
}

func (w *Writer) handleParty(inst Instruction) bool {
	switch inst.Op {
	case Put:
		return sdk.Put(w.Svc, getPartyTable(), inst.Party)
	case Remove:
		input := &dynamodb.DeleteItemInput{
			Key: map[string]*dynamodb.AttributeValue{
				"name": {
					S: aws.String(inst.Party.Name),
				},
				"dm": {
					S: aws.String(inst.Party.DM),
				},
			},
			TableName: aws.String(getPartyTable()),
		}

		return sdk.DeleteItem(w.Svc, input)
	case Modify:
		log.Println("party to modify", inst.Party)
		updateInput := makeUpdatePartyInput(inst)
		_, ok := sdk.UpdateItem(w.Svc, updateInput)
		return ok
	default:
		log.Println("invalid operation", inst)
		return false
	}
}

func makeUpdatePartyInput(inst Instruction) *dynamodb.UpdateItemInput {
	soft := inst.Party.Name != ""
	expressionAttributeNames := map[string]*string{}
	expressionAttributeValues := map[string]*dynamodb.AttributeValue{}
	key := map[string]*dynamodb.AttributeValue{}
	key["name"] = &dynamodb.AttributeValue{S: aws.String(inst.Party.Name)}
	key["dm"] = &dynamodb.AttributeValue{S: aws.String(inst.Party.DM)}

	var updateExpression string

	if soft {
		nameExpression := "#NAME_EXPRESSION = :nameExpression"
		expressionAttributeNames["#NAME_EXPRESSION"] = aws.String("name")
		expressionAttributeValues[":nameExpression"] = &dynamodb.AttributeValue{S: aws.String(inst.Party.Name)}

		dmExpression := "#DM_EXPRESSION = :dmExpression"
		expressionAttributeNames["#DM_EXPRESSION"] = aws.String("dm")
		expressionAttributeValues[":dmExpression"] = &dynamodb.AttributeValue{S: aws.String(inst.Party.DM)}

		membersExpression := "#MEMBERS_EXPRESSION = :membersExpression"
		expressionAttributeNames["#MEMBERS_EXPRESSION"] = aws.String("members")
		expressionAttributeValues[":membersExpression"] = &dynamodb.AttributeValue{SS: aws.StringSlice(inst.Party.Members)}

		updateExpression = fmt.Sprintf("SET %s, %s, %s", nameExpression, dmExpression, membersExpression)
	} else {
		passwordExpression := "#PASSWORD_EXPRESSION = :passwordExpression"
		expressionAttributeNames["#PASSWORD_EXPRESSION"] = aws.String("password")
		expressionAttributeValues[":passwordExpression"] = &dynamodb.AttributeValue{S: aws.String(inst.Party.Password)}

		idExpression := "#ID_EXPRESSION = :idExpression"
		expressionAttributeNames["#ID_EXPRESSION"] = aws.String("id")
		expressionAttributeValues[":idExpression"] = &dynamodb.AttributeValue{S: aws.String(inst.Party.Id)}

		updateExpression = fmt.Sprintf("SET %s, %s", passwordExpression, idExpression)
	}

	updateInput := &dynamodb.UpdateItemInput{
		ExpressionAttributeNames:  expressionAttributeNames,
		ExpressionAttributeValues: expressionAttributeValues,
		Key:                       key,
		ReturnValues:              aws.String("ALL_NEW"),
		TableName:                 aws.String(getPartyTable()),
		UpdateExpression:          aws.String(updateExpression),
	}
	return updateInput
}

func cleanWriter(c chan Instruction, f func(i Instruction)) {
	close(c)
	for inst := range c {
		f(inst)
	}
}

func (w *Writer) CleanUp() {
	wg := sync.WaitGroup{}

	//TODO: handle delete
	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		cleanWriter(w.Bridge.InUser, w.processUser)
	}(&wg)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		cleanWriter(w.Bridge.InItem, w.processItem)
	}(&wg)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		cleanWriter(w.Bridge.InParty, w.processParty)
	}(&wg)

	wg.Wait()
	log.Println("write channels cleaned")
}
