package io

type Bridge struct {
	InUser   chan Instruction
	InItem   chan Instruction
	InParty  chan Instruction
	OutUser  chan Instruction
	OutItem  chan Instruction
	OutParty chan Instruction
}

func (b *Bridge) Init() {
	b.InUser = make(chan Instruction, getChanLen())
	b.InItem = make(chan Instruction, getChanLen())
	b.InParty = make(chan Instruction, getChanLen())
	b.OutUser = make(chan Instruction, getChanLen())
	b.OutItem = make(chan Instruction, getChanLen())
	b.OutParty = make(chan Instruction, getChanLen())
}
