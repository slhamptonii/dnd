package io

import (
	"fmt"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
	"log"
	"math"
	"os"
	"strings"
)

const table = "sheldonsandbox_%s_qry_%s"
const queue = "sheldonsandbox-%s-cmd-%s.fifo"

var dynamodbSvc dynamodbiface.DynamoDBAPI
var sqsSvc sqsiface.SQSAPI
var env string
var userTable string
var itemTable string
var partyTable string
var userQueue string
var itemQueue string
var partyQueue string

func getEnv() string {
	if env == "" {
		env = strings.ToLower(os.Getenv("ENVIRONMENT"))
	}

	return env
}

func getUserTable() string {
	if userTable == "" {
		userTable = fmt.Sprintf(table, getEnv(), "user")
	}

	return userTable
}

func getItemTable() string {
	if itemTable == "" {
		itemTable = fmt.Sprintf(table, getEnv(), "item")
	}

	return itemTable
}

func getPartyTable() string {
	if partyTable == "" {
		partyTable = fmt.Sprintf(table, getEnv(), "party")
	}

	return partyTable
}

func getUserQueue() string {
	if userQueue == "" {
		userQueue = fmt.Sprintf(queue, getEnv(), "user")
	}

	return userQueue
}

func getItemQueue() string {
	if itemQueue == "" {
		itemQueue = fmt.Sprintf(queue, getEnv(), "item")
	}

	return itemQueue
}

func getPartyQueue() string {
	if partyQueue == "" {
		partyQueue = fmt.Sprintf(queue, getEnv(), "party")
	}

	return partyQueue
}

func dynamodbConnect() dynamodbiface.DynamoDBAPI {
	if dynamodbSvc == nil {
		dynamodbSvc = sdk.DynamoConnect()
	}

	return dynamodbSvc
}

func sqsConnect() sqsiface.SQSAPI {
	log.Println("connecting to sqs")
	if sqsSvc == nil {
		sqsSvc = sdk.SQSConnect()
	}

	return sqsSvc
}

func getChanLen() int {
	switch getEnv() {
	case "PROD":
		return math.MaxInt32
	default:
		return math.MaxInt8
	}
}
