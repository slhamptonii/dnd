package io

import (
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/stretchr/testify/assert"
	"math"
	"testing"
)

func TestGetEnv(t *testing.T) {
	assert.Equal(t, "local", getEnv())
}

func TestGetUserTable(t *testing.T) {
	assert.Equal(t, "sheldonsandbox_local_qry_user", getUserTable())
}

func TestGetItemTable(t *testing.T) {
	assert.Equal(t, "sheldonsandbox_local_qry_item", getItemTable())
}

func TestGetPartyTable(t *testing.T) {
	assert.Equal(t, "sheldonsandbox_local_qry_party", getPartyTable())
}

func TestGetUserQueue(t *testing.T) {
	assert.Equal(t, "sheldonsandbox-local-cmd-user.fifo", getUserQueue())
}

func TestGetItemQueue(t *testing.T) {
	assert.Equal(t, "sheldonsandbox-local-cmd-item.fifo", getItemQueue())
}

func TestGetPartyQueue(t *testing.T) {
	assert.Equal(t, "sheldonsandbox-local-cmd-party.fifo", getPartyQueue())
}

func TestDynamodbConnect(t *testing.T) {
	svc := dynamodbConnect()
	_, ok := svc.(dynamodbiface.DynamoDBAPI)
	assert.Equal(t, true, ok)
}

func TestSQSConnect(t *testing.T) {
	svc := sqsConnect()
	_, ok := svc.(sqsiface.SQSAPI)
	assert.Equal(t, true, ok)
}

func TestGetChanLen(t *testing.T) {
	assert.Equal(t, math.MaxInt8, getChanLen())
}
