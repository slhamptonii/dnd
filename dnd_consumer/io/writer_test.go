package io

import (
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"testing"
)

func TestWriterConnect(t *testing.T) {
	w := Writer{
		Bridge: nil,
		Svc:    nil,
	}

	assert.True(t, w.Connect())
	assert.NotNil(t, w.Svc)
}

func TestCleanWriter(t *testing.T) {
	n := 0
	c := make(chan Instruction, 3)
	f := func(i Instruction) {
		assert.Equal(t, "foo", i.Id)
		n++
	}

	m := 3
	i := 0

	for i < m {
		c <- Instruction{Id: "foo"}
		i++
	}

	cleanWriter(c, f)
	assert.Equal(t, m, n)
}

func TestWriterHandleUser(t *testing.T) {
	m := &mockDynamoOutItem{}
	w := Writer{
		Svc: m,
	}
	inst := Instruction{
		Op:   Put,
		User: model.User{},
	}

	assert.True(t, w.handleUser(inst))

	m.Err = errors.New("test error")
	assert.False(t, w.handleUser(inst))
}

type mockDynamoOutItem struct {
	dynamodbiface.DynamoDBAPI
	Err error
}

func (m *mockDynamoOutItem) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	return &dynamodb.PutItemOutput{}, m.Err
}
