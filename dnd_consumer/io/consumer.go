package io

import (
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/google/uuid"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
	"log"
	"strings"
	"sync"
)

type Consumer struct {
	UserUrl  string
	ItemUrl  string
	PartyUrl string
	Bridge   *Bridge
	Svc      sqsiface.SQSAPI
}

func (c *Consumer) Connect() bool {
	c.Svc = sqsConnect()
	return c.GetUrls()
}

func (c *Consumer) GetUrls() bool {
	return c.getUserUrl() && c.getItemUrl() && c.getPartyUrl()
}

func (c *Consumer) Consume() {
	go c.consumeUser()
	go c.consumeItem()
	go c.consumeParty()
}

func (c *Consumer) Process() {
	for {
		select {
		//TODO: handle delete
		case inst := <-c.Bridge.InUser:
			log.Printf("received user to delete from queue")
			c.processUser(inst.Id)
		case inst := <-c.Bridge.InItem:
			c.processItem(inst.Id)
		case inst := <-c.Bridge.InParty:
			c.processParty(inst.Id)
		}
	}
}

func (c *Consumer) processParty(id string) {
	removeAndDelete(c.Svc, c.PartyUrl, id)
}

func (c *Consumer) processItem(id string) {
	removeAndDelete(c.Svc, c.ItemUrl, id)
}

func (c *Consumer) processUser(id string) {
	removeAndDelete(c.Svc, c.UserUrl, id)
}

func removeAndDelete(svc sqsiface.SQSAPI, qUrl string, id string) {
	it := Store().Get(id)
	handle, ok := it.(string)
	if !ok {
		log.Println("could not get handle from store")
		return
	}

	ok = sdk.DeleteMessage(svc, &sqs.DeleteMessageInput{
		QueueUrl:      aws.String(qUrl),
		ReceiptHandle: aws.String(handle),
	})
	if !ok {
		//TODO: send to dlq
		log.Println("could not delete message", handle)
		return
	} else {
		log.Println("successfully deleted message", handle)
	}

	Store().Remove(id)
}

func (c *Consumer) consumeUser() {
	for {
		messages := sdk.ReceiveMessage(c.Svc, &sqs.ReceiveMessageInput{
			AttributeNames: aws.StringSlice([]string{
				"SentTimestamp",
			}),
			MaxNumberOfMessages: aws.Int64(10),
			MessageAttributeNames: []*string{
				aws.String(sqs.QueueAttributeNameAll),
			},
			QueueUrl:                aws.String(c.UserUrl),
			ReceiveRequestAttemptId: aws.String(uuid.New().String()),
		})

		c.handleUserMessages(messages)
	}
}

func (c *Consumer) consumeItem() {
	for {
		messages := sdk.ReceiveMessage(c.Svc, &sqs.ReceiveMessageInput{
			AttributeNames: aws.StringSlice([]string{
				"SentTimestamp",
			}),
			MaxNumberOfMessages: aws.Int64(10),
			MessageAttributeNames: []*string{
				aws.String(sqs.QueueAttributeNameAll),
			},
			QueueUrl:                aws.String(c.ItemUrl),
			ReceiveRequestAttemptId: aws.String(uuid.New().String()),
		})

		c.handleItemMessages(messages)
	}
}

func (c *Consumer) consumeParty() {
	for {
		messages := sdk.ReceiveMessage(c.Svc, &sqs.ReceiveMessageInput{
			AttributeNames: aws.StringSlice([]string{
				"SentTimestamp",
			}),
			MaxNumberOfMessages: aws.Int64(10),
			MessageAttributeNames: []*string{
				aws.String(sqs.QueueAttributeNameAll),
			},
			QueueUrl:                aws.String(c.PartyUrl),
			ReceiveRequestAttemptId: aws.String(uuid.New().String()),
		})

		c.handlePartyMessages(messages)
	}
}

func (c *Consumer) handleUserMessages(messages []*sqs.Message) {
	for _, m := range messages {
		var op Option
		if !validateMsgAttr("user", &op, m) {
			//TODO: push to dlq
			log.Println("invalid message attributes ", m.MessageAttributes)
			continue
		}

		inst := Instruction{
			Status: 0,
			Id:     *m.MessageId,
			Op:     op,
			It:     nil,
		}
		log.Println("message body", *m.Body)
		bodyBytes := []byte(*m.Body)
		u := model.User{}

		err := json.Unmarshal(bodyBytes, &u)
		if err != nil {
			log.Println("could not unmarshal user", err)
			continue
		}
		inst.User = u
		log.Printf("sending %s to bridge \n", inst.Id)
		c.Bridge.OutUser <- inst

		Store().Insert(inst.Id, *m.ReceiptHandle)
	}
}

func (c *Consumer) handleItemMessages(messages []*sqs.Message) {
	for _, m := range messages {
		var op Option
		if !validateMsgAttr("item", &op, m) {
			//TODO: push to dlq
			log.Println("invalid message attributes")
			continue
		}

		inst := Instruction{
			Status: 0,
			Id:     *m.MessageId,
			Op:     op,
			It:     nil,
		}
		bodyBytes := []byte(*m.Body)
		i := model.Item{}

		err := json.Unmarshal(bodyBytes, &i)
		if err != nil {
			log.Println("could not unmarshal item", err)
			continue
		}
		inst.Item = i
		log.Printf("sending %s to bridge \n", inst.Id)
		c.Bridge.OutItem <- inst

		Store().Insert(inst.Id, *m.ReceiptHandle)
	}
}

func (c *Consumer) handlePartyMessages(messages []*sqs.Message) {
	for _, m := range messages {
		var op Option
		if !validateMsgAttr("party", &op, m) {
			//TODO: push to dlq
			log.Println("invalid message attributes")
			continue
		}

		inst := Instruction{
			Status: 0,
			Id:     *m.MessageId,
			Op:     op,
			It:     nil,
		}
		bodyBytes := []byte(*m.Body)
		p := model.Party{}

		err := json.Unmarshal(bodyBytes, &p)
		if err != nil {
			log.Println("could not unmarshal party", err)
			continue
		}
		inst.Party = p
		log.Printf("sending %s to bridge \n", inst.Id)
		c.Bridge.OutParty <- inst

		Store().Insert(inst.Id, *m.ReceiptHandle)
	}
}

func validateMsgAttr(table string, op *Option, m *sqs.Message) bool {
	msgAtt := extractMessageAttributes(m)
	log.Println("message attributes", msgAtt)
	if msgAtt == nil || strings.ToLower(msgAtt[0]) != getEnv() || msgAtt[1] != table {
		return false
	}

	switch msgAtt[2] {
	case "INSERT":
		*op = Put
	case "REMOVE":
		*op = Remove
	case "MODIFY":
		*op = Modify
	default:
		log.Println("operation not allowed", msgAtt[2])
		return false
	}

	return true
}

func (c *Consumer) CleanUp() {
	wg := sync.WaitGroup{}

	//TODO: handle delete
	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		cleanConsumer(c.Bridge.InUser, c.processUser)
	}(&wg)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		cleanConsumer(c.Bridge.InItem, c.processItem)
	}(&wg)

	wg.Add(1)
	go func(wg *sync.WaitGroup) {
		defer wg.Done()
		cleanConsumer(c.Bridge.InParty, c.processParty)
	}(&wg)

	wg.Wait()
	log.Println("write channels cleaned")
}

func extractMessageAttributes(msg *sqs.Message) []string {
	if msg == nil {
		log.Println("no messages to extract")
		return nil
	}

	env := msg.MessageAttributes["Environment"]
	table := msg.MessageAttributes["Table"]
	action := msg.MessageAttributes["Action"]

	if env == nil || table == nil || action == nil {
		log.Println("invalid message attributes", msg.MessageAttributes)
		return nil
	}

	return []string{
		*env.StringValue,
		*table.StringValue,
		*action.StringValue,
	}
}

func (c *Consumer) getUserUrl() bool {
	c.UserUrl = sdk.GetQueueUrl(c.Svc, &sqs.GetQueueUrlInput{
		QueueName: aws.String(getUserQueue()),
	})

	return c.UserUrl != ""
}

func (c *Consumer) getItemUrl() bool {
	c.ItemUrl = sdk.GetQueueUrl(c.Svc, &sqs.GetQueueUrlInput{
		QueueName: aws.String(getItemQueue()),
	})

	return c.ItemUrl != ""
}

func (c *Consumer) getPartyUrl() bool {
	c.PartyUrl = sdk.GetQueueUrl(c.Svc, &sqs.GetQueueUrlInput{
		QueueName: aws.String(getPartyQueue()),
	})

	return c.PartyUrl != ""
}

func cleanConsumer(c chan Instruction, f func(id string)) {
	close(c)
	for inst := range c {
		f(inst.Id)
	}
}
