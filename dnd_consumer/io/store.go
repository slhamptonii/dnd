package io

import "github.com/golang-collections/collections/trie"

var store *trie.Trie

func Store() *trie.Trie {
	if store == nil {
		store = trie.New()
		store.Init()
	}
	return store
}
