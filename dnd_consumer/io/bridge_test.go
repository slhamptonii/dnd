package io

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBridgeInit(t *testing.T) {
	b := Bridge{}
	b.Init()

	assert.NotNil(t, b.InUser)
	assert.NotNil(t, b.InItem)
	assert.NotNil(t, b.InParty)
	assert.NotNil(t, b.OutUser)
	assert.NotNil(t, b.OutItem)
	assert.NotNil(t, b.OutParty)
}
