#!/bin/bash

repo_uri=$1
if [[ -z "$repo_uri" ]]; then
  echo "need a repository to push to"
  exit 1
fi

echo "building executable"
bash go-executable-build.bash dnd-consumer ./main

echo "building image"
docker build --rm -t "dnd-consumer" --build-arg ex_path=linux/amd64 .

echo "logging in to aws"
$(aws ecr get-login --no-include-email --region us-west-2 --profile tharivol)

echo "tagging image"
docker tag dnd-consumer:latest $repo_uri

echo "pushing image"
docker push $repo_uri:latest

echo "deploying"
aws ecs update-service --cluster sandbox-cluster --service dnd-consumer-dev-service --desired-count 1 --force-new-deployment

echo "Finito!"