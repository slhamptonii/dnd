module gitlab.com/slhamptonii/dnd_sender

go 1.14

require (
	github.com/aws/aws-lambda-go v1.20.0 // indirect
	github.com/aws/aws-sdk-go v1.36.0
	github.com/golang-collections/collections v0.0.0-20130729185459-604e922904d3
	github.com/google/uuid v1.1.2
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1
	gitlab.com/slhamptonii/sheldonsandbox-core/v2 v2.7.0
)
