module gitlab.com/slhamptonii/dnd/cmd_users

go 1.14

require (
	github.com/aws/aws-lambda-go v1.20.0
	github.com/aws/aws-sdk-go v1.36.8
	github.com/stretchr/testify v1.6.1
	gitlab.com/slhamptonii/sheldonsandbox-core/v2 v2.8.0
	golang.org/x/crypto v0.0.0-20201208171446-5f87f3452ae9 // indirect
)
