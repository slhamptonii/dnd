package main

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"log"
	"net/http"
	"strconv"
	"testing"
)

var er = errors.New("test error")

func TestA(t *testing.T) {
	log.Println(*aws.String(strconv.Itoa(int(model.Player))))
}

func TestHandleRequest(t *testing.T) {
	//405
	res, err := HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusMethodNotAllowed, res.StatusCode)
	assert.Equal(t, "nah, bro", res.Body)

	//Fail - no token
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusForbidden, res.StatusCode)
	assert.Equal(t, "invalid sender", res.Body)

	//Delete Fail
	dynamoSvc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "DELETE",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/someone",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusInternalServerError, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "could not delete user", res.Body)

	//Delete Success
	dynamoSvc = &mockOutItem{}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "DELETE",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/someone",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusNoContent, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "", res.Body)

	//Create Fail - bad json
	dynamoSvc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/someone",
		Body: "!@#$%^&*()",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "invalid payload", res.Body)

	//Create Fail - bad dynamo put
	dynamoSvc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/someone",
		Body: "{\"username\": \"penelope\"}",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusInternalServerError, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "could not save user", res.Body)

	//Create Success
	dynamoSvc = &mockOutItem{}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/someone",
		Body: "{\"username\": \"penelope\"}",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusCreated, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "{\"username\":\"penelope\",\"password\":\"\",\"role\":1,\"party\":\"\"}", res.Body)
}

func TestPutUser(t *testing.T) {
	//Success
	dynamoSvc = &mockOutItem{}
	ok := PutUser(model.User{})
	assert.Equal(t, true, ok)

	//Fail
	dynamoSvc = &mockOutItem{Err: er}
	ok = PutUser(model.User{})
	assert.Equal(t, false, ok)
}

func TestDeleteUser(t *testing.T) {
	//Success
	dynamoSvc = &mockOutItem{}
	ok := DeleteUser("somebody")
	assert.Equal(t, true, ok)

	//Fail
	dynamoSvc = &mockOutItem{Err: er}
	ok = DeleteUser("nobody")
	assert.Equal(t, false, ok)
}

type mockOutItem struct {
	dynamodbiface.DynamoDBAPI
	Err error
}

func (m *mockOutItem) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	return &dynamodb.PutItemOutput{}, m.Err
}

func (m mockOutItem) DeleteItem(input *dynamodb.DeleteItemInput) (*dynamodb.DeleteItemOutput, error) {
	return &dynamodb.DeleteItemOutput{}, m.Err
}
