package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	lam "github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/aws/aws-sdk-go/service/lambda/lambdaiface"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/crypt"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	request "gitlab.com/slhamptonii/sheldonsandbox-core/v2/req"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
)

var tableName = os.Getenv("TABLE_NAME")
var dynamoSvc dynamodbiface.DynamoDBAPI
var lambdaSvc lambdaiface.LambdaAPI

type PasswordChange struct {
	Username         string     `json:"username"`
	Role             model.Role `json:"role"`
	OriginalPassword string     `json:"originalPassword"`
	NewPassword      string     `json:"newPassword"`
}

func init() {
	dynamoSvc = sdk.DynamoConnect()
	lambdaSvc = sdk.LambdaConnect()
}

func HandleRequest(r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	ss := sdk.PullFromCtx(&r.RequestContext, "ss")[0]
	signedString, ok := ss.(string)
	if !ok {
		return sdk.NoAuthRes(http.StatusForbidden, "invalid sender")
	}
	headers := map[string]string{"x-ss": signedString}
	headers["content-type"] = "application/json"
	headers["access-control-allow-origin"] = "*"
	headers["access-control-expose-headers"] = "x-ss"
	headers["accept"] = "*/*"

	switch r.HTTPMethod {
	case "POST":
		var user model.User
		err := json.Unmarshal([]byte(r.Body), &user)
		if err != nil {
			log.Println(r.RequestContext.RequestID, "could not unmarshal payload", err)
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		user.Role = model.Player
		//TODO: set config for this
		user.Password = crypt.HashAndSaltDefault([]byte(user.Password))

		ok := PutUser(user)
		if !ok {
			msg := "could not save user"
			return sdk.ShortRes(http.StatusInternalServerError, headers, msg)
		}

		user.Password = ""
		b, err := json.Marshal(user)
		if err != nil {
			log.Println("could not marshal user", err)
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		return sdk.ShortRes(http.StatusCreated, headers, string(b))
	case "DELETE":
		username := strings.Trim(strings.Split(r.Path, "/")[2], "\r\n ")
		ok := DeleteUser(username)
		if !ok {
			msg := "could not delete user"
			return sdk.ShortRes(http.StatusInternalServerError, headers, msg)
		}
		return sdk.ShortRes(http.StatusNoContent, headers, "")
	case "PATCH":
		vars := request.ExtractPath(r.Path)
		username := vars[1]
		_, ok := findUser(username, signedString)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "unable to process user update")
		}

		var newUser model.User
		err := json.Unmarshal([]byte(r.Body), &newUser)
		if err != nil {
			log.Println(r.RequestContext.RequestID, "could not unmarshal payload", err)
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		expressionAttributeNames := map[string]*string{}
		expressionAttributeValues := map[string]*dynamodb.AttributeValue{}
		key := map[string]*dynamodb.AttributeValue{}
		key["username"] = &dynamodb.AttributeValue{S: aws.String(newUser.Username)}
		key["role"] = &dynamodb.AttributeValue{N: aws.String(strconv.Itoa(int(newUser.Role)))}

		partyExpression := "#PARTIES_EXPRESSION = :partiesExpression"
		expressionAttributeNames["#PARTIES_EXPRESSION"] = aws.String("parties")
		expressionAttributeValues[":partiesExpression"] = &dynamodb.AttributeValue{L: convertPartiesToAWS(newUser.Parties)}

		updateInput := &dynamodb.UpdateItemInput{
			ExpressionAttributeNames:  expressionAttributeNames,
			ExpressionAttributeValues: expressionAttributeValues,
			Key:                       key,
			ReturnValues:              aws.String("ALL_NEW"),
			TableName:                 aws.String(tableName),
			UpdateExpression:          aws.String(fmt.Sprintf("SET %s", partyExpression)),
		}

		_, ok = sdk.UpdateItem(dynamoSvc, updateInput)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "unable to process user update")
		}

		b, err := json.Marshal(newUser)
		if err != nil {
			log.Println("could not marshal user", err)
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		return sdk.ShortRes(http.StatusOK, headers, string(b))
	case "PUT":
		vars := request.ExtractPath(r.Path)
		username := vars[1]

		user, ok := findUser(username, signedString)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "unable to process user update")
		}

		var passwordChange PasswordChange
		err := json.Unmarshal([]byte(r.Body), &passwordChange)
		if err != nil {
			log.Println("could not unmarshal payload", err)
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		if !crypt.ComparePasswords(user.Password, []byte(passwordChange.OriginalPassword)) {
			log.Println("invalid old password")
			return sdk.ShortRes(http.StatusForbidden, headers, "invalid old password")
		}

		expressionAttributeNames := map[string]*string{}
		expressionAttributeValues := map[string]*dynamodb.AttributeValue{}
		key := map[string]*dynamodb.AttributeValue{}
		key["username"] = &dynamodb.AttributeValue{S: aws.String(passwordChange.Username)}
		key["role"] = &dynamodb.AttributeValue{N: aws.String(strconv.Itoa(int(passwordChange.Role)))}

		passwordExpression := "#PASSWORD_EXPRESSION = :passwordExpression"
		expressionAttributeNames["#PASSWORD_EXPRESSION"] = aws.String("password")
		expressionAttributeValues[":passwordExpression"] = &dynamodb.AttributeValue{S: aws.String(crypt.HashAndSaltDefault([]byte(passwordChange.NewPassword)))}

		updateInput := &dynamodb.UpdateItemInput{
			ExpressionAttributeNames:  expressionAttributeNames,
			ExpressionAttributeValues: expressionAttributeValues,
			Key:                       key,
			ReturnValues:              aws.String("ALL_NEW"),
			TableName:                 aws.String(tableName),
			UpdateExpression:          aws.String(fmt.Sprintf("SET %s", passwordExpression)),
		}

		_, ok = sdk.UpdateItem(dynamoSvc, updateInput)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "unable to process user update")
		}

		passwordChange.OriginalPassword = ""
		passwordChange.NewPassword = ""
		b, err := json.Marshal(passwordChange)
		if err != nil {
			log.Println("could not marshal user", err)
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		return sdk.ShortRes(http.StatusOK, headers, string(b))
	default:
		return sdk.NoAuthRes(http.StatusMethodNotAllowed, "nah, bro")
	}
}

func PutUser(user model.User) bool {
	av, err := dynamodbattribute.MarshalMap(user)
	if err != nil {
		log.Println("could not marshal user", err)
		return false
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}

	return sdk.PutItem(dynamoSvc, input)
}

func DeleteUser(username string) bool {
	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"username": {
				S: aws.String(username),
			},
			"role": {
				N: aws.String(strconv.Itoa(int(model.Player))),
			},
		},
		TableName: aws.String(tableName),
	}

	return sdk.DeleteItem(dynamoSvc, input)
}

func findUser(username, signedString string) (model.User, bool) {
	var user model.User
	log.Println("searching for", username)
	path := fmt.Sprintf("/users/%s", username)
	req := events.APIGatewayProxyRequest{
		Path:       path,
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{
			Identity:   events.APIGatewayRequestIdentity{},
			Authorizer: map[string]interface{}{"ss": signedString},
		},
	}

	payload, err := json.Marshal(req)
	if err != nil {
		log.Println("could not marshal lambda request ", err)
		return user, false
	}

	invokeInput := &lambda.InvokeInput{
		FunctionName: aws.String("sheldonsandbox-qry-users"),
		Payload:      payload,
	}

	if output, ok := sdk.Invoke(lambdaSvc, invokeInput); !ok {
		log.Println("function invoke failed ", *output.FunctionError)
		return user, false
	} else {
		var res events.APIGatewayProxyResponse
		err = json.Unmarshal(output.Payload, &res)
		if err != nil {
			log.Println("could not unmarshal response", err)
			return user, false
		}

		err = json.Unmarshal([]byte(res.Body), &user)
		if err != nil {
			log.Println("could not unmarshal body into user", res.Body, err)
			return user, false
		}
	}
	return user, true
}

func convertPartiesToAWS(parties [][]string) []*dynamodb.AttributeValue {
	values := make([]*dynamodb.AttributeValue, 0)
	for _, arr := range parties {
		name, dm := arr[0], arr[1]
		pair := dynamodb.AttributeValue{
			L: &dynamodb.AttributeValue{S: name},
			L: dynamodb.AttributeValue{S, dm},
		}
		values = append(values, &pair)
	}

	return values
}

func main() {
	lam.Start(HandleRequest)
}
