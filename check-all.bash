#!/bin/bash

for f in *; do
    if [ -d "${f}" ]; then
        (
          bash check.bash "$f"
        )
    fi
done
