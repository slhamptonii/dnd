#!/bin/bash

(
  cd "$1" || exit &&
  go get -u ./...
  go mod tidy &&
  go mod vendor &&
  go fmt ./... &&
  go test -covermode atomic -coverprofile .coverage.out ./... &&
  go tool cover -func .coverage.out
)
