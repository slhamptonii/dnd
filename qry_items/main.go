package main

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	request "gitlab.com/slhamptonii/sheldonsandbox-core/v2/req"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
	"log"
	"net/http"
	"os"
)

var tableName string
var svc dynamodbiface.DynamoDBAPI

func init() {
	tableName = os.Getenv("ITEM_TABLE_NAME")
	svc = sdk.DynamoConnect()
}

func HandleRequest(r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	ss := sdk.PullFromCtx(&r.RequestContext, "ss")[0]
	signedString, ok := ss.(string)
	if !ok {
		return sdk.NoAuthRes(http.StatusForbidden, "invalid sender")
	}
	headers := map[string]string{"x-ss": signedString}
	headers["content-type"] = "application/json"
	headers["access-control-allow-origin"] = "*"
	headers["access-control-expose-headers"] = "x-ss"
	headers["accept"] = "*/*"

	if r.HTTPMethod == "GET" {
		vars := request.ExtractPath(r.Path)
		size := len(vars)
		owner := vars[1]

		if size == 4 {
			id := vars[3]
			item := GetItem(id, owner)
			if item.Owner != owner || item.Id != id {
				return sdk.ShortRes(http.StatusNotFound, headers, "ain't here boss")
			}

			iString, err := json.Marshal(item)
			if err != nil {
				return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
			}
			return sdk.ShortRes(http.StatusOK, headers, string(iString))
		} else {
			items := GetItems(owner)
			if items == nil {
				return sdk.ShortRes(http.StatusNotFound, headers, "ain't here boss")
			}

			iString, err := json.Marshal(items)
			if err != nil {
				return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
			}
			return sdk.ShortRes(http.StatusOK, headers, string(iString))
		}
	} else {
		return sdk.NoAuthRes(http.StatusMethodNotAllowed, "nah, bro")
	}
}

func GetItem(id, owner string) model.Item {
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"id": {
				S: aws.String(id),
			},
			"owner": {
				S: aws.String(owner),
			},
		},
	})
	if err != nil {
		log.Println("error calling GetItem", err)
		return model.Item{}
	}

	item := model.Item{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		log.Println("could not unmarshal item", err)
		return model.Item{}
	}

	return item
}

//TODO: scan -> query
func GetItems(owner string) []model.Item {
	filt := expression.Name("owner").Equal(expression.Value(owner))
	proj := expression.NamesList(
		expression.Name("id"), expression.Name("enchantment"),
		expression.Name("name"), expression.Name("description"), expression.Name("owner"))

	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()
	if err != nil {
		log.Println("could not build item query", err)
		return nil
	}

	result, err := svc.Scan(&dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		Limit:                     nil, //TODO: add pagination
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(tableName),
	})
	if err != nil {
		log.Println("could not query items", err)
		return nil
	}

	var items []model.Item
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &items)
	if err != nil {
		log.Println("could not unmarshal items", err)
		return nil
	}

	return items
}

func main() {
	lambda.Start(HandleRequest)
}
