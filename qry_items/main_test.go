package main

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

var er = errors.New("test error")

func TestHandleRequest(t *testing.T) {
	//Fail - 405
	res, err := HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusMethodNotAllowed, res.StatusCode)
	assert.Equal(t, "nah, bro", res.Body)

	//Fail - no token
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusForbidden, res.StatusCode)
	assert.Equal(t, "invalid sender", res.Body)

	//Fail - Get Item - could not find item
	svc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/Testie/items/lettersandnumbers",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusNotFound, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "ain't here boss", res.Body)

	//Success - Get Item
	svc = &mockOutItem{Item: map[string]*dynamodb.AttributeValue{
		"Id":          {S: aws.String("lettersandnumbers")},
		"Name":        {S: aws.String("rock")},
		"Description": {S: aws.String("smash smash smash")},
		"Owner":       {S: aws.String("Testie")},
	},
		Err: nil,
	}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/Testie/items/lettersandnumbers",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "{\"id\":\"lettersandnumbers\",\"name\":\"rock\",\"description\":\"smash smash smash\",\"owner\":\"Testie\"}", res.Body)

	//Fail - Get Items - could not find items
	svc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/Testie/items",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusNotFound, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "ain't here boss", res.Body)

	//Success - Get Item
	svc = &mockOutItem{Items: []map[string]*dynamodb.AttributeValue{
		{
			"Id":          {S: aws.String("lettersandnumbers")},
			"Name":        {S: aws.String("rock")},
			"Description": {S: aws.String("smash smash smash")},
			"Owner":       {S: aws.String("Testie")},
		},
		{
			"Id":          {S: aws.String("numbersandletters")},
			"Name":        {S: aws.String("axe")},
			"Description": {S: aws.String("in two!")},
			"Owner":       {S: aws.String("Testie")},
		},
	},
		Err: nil,
	}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/Testie/items",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "[{\"id\":\"lettersandnumbers\",\"name\":\"rock\",\"description\":\"smash smash smash\",\"owner\":\"Testie\"},{\"id\":\"numbersandletters\",\"name\":\"axe\",\"description\":\"in two!\",\"owner\":\"Testie\"}]", res.Body)
}

func TestGetItem(t *testing.T) {
	//Success
	svc = &mockOutItem{Item: map[string]*dynamodb.AttributeValue{
		"Id":          {S: aws.String("lettersandnumbers")},
		"Name":        {S: aws.String("rock")},
		"Description": {S: aws.String("smash smash smash")},
		"Owner":       {S: aws.String("Testie")},
	},
		Err: nil,
	}
	item := GetItem("lettersandnumbers", "Testie")
	assert.Equal(t, "lettersandnumbers", item.Id)
	assert.Equal(t, "rock", item.Name)
	assert.Equal(t, "smash smash smash", item.Description)
	assert.Equal(t, "Testie", item.Owner)

	//Fail - could not find item
	svc = &mockOutItem{Err: er}
	item = GetItem("lettersandnumbers", "Testie")
	assert.Equal(t, "", item.Id)
	assert.Equal(t, "", item.Name)
	assert.Equal(t, "", item.Description)
	assert.Equal(t, "", item.Owner)
}

func TestGetItems(t *testing.T) {
	svc = &mockOutItem{Items: []map[string]*dynamodb.AttributeValue{
		{
			"Id":          {S: aws.String("lettersandnumbers")},
			"Name":        {S: aws.String("rock")},
			"Description": {S: aws.String("smash smash smash")},
			"Owner":       {S: aws.String("Testie")},
		},
		{
			"Id":          {S: aws.String("numbersandletters")},
			"Name":        {S: aws.String("axe")},
			"Description": {S: aws.String("in two!")},
			"Owner":       {S: aws.String("Testie")},
		},
	},
		Err: nil,
	}
	items := GetItems("Testie")
	assert.Equal(t, 2, len(items))
	assert.Equal(t, "lettersandnumbers", items[0].Id)
	assert.Equal(t, "rock", items[0].Name)
	assert.Equal(t, "smash smash smash", items[0].Description)
	assert.Equal(t, "Testie", items[0].Owner)
	assert.Equal(t, "numbersandletters", items[1].Id)
	assert.Equal(t, "axe", items[1].Name)
	assert.Equal(t, "in two!", items[1].Description)
	assert.Equal(t, "Testie", items[1].Owner)

	svc = &mockOutItem{Err: er}
	items = GetItems("Testie")
	assert.Equal(t, 0, len(items))
}

type mockOutItem struct {
	dynamodbiface.DynamoDBAPI
	Item  map[string]*dynamodb.AttributeValue
	Items []map[string]*dynamodb.AttributeValue
	Err   error
}

func (m mockOutItem) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {
	return &dynamodb.GetItemOutput{Item: m.Item}, m.Err
}

func (m mockOutItem) Scan(input *dynamodb.ScanInput) (*dynamodb.ScanOutput, error) {
	return &dynamodb.ScanOutput{Items: m.Items}, m.Err
}
