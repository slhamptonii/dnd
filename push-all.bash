#!/bin/bash

for f in *; do
    if [ -d "${f}" ]; then
        (
          cd "$f" || exit &&
          sh updateLambda.sh
        )
    fi
done
