package main

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
	"log"
	"net/http"
	"os"
	"strings"
)

var tableName = os.Getenv("TABLE_NAME")
var svc dynamodbiface.DynamoDBAPI

func init() {
	svc = sdk.DynamoConnect()
}

func HandleRequest(r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	ss := sdk.PullFromCtx(&r.RequestContext, "ss")[0]
	signedString, ok := ss.(string)
	if !ok {
		return sdk.NoAuthRes(http.StatusForbidden, "invalid sender")
	}
	headers := map[string]string{"x-ss": signedString}
	headers["content-type"] = "application/json"
	headers["access-control-allow-origin"] = "*"
	headers["access-control-expose-headers"] = "x-ss"
	headers["accept"] = "*/*"

	if r.HTTPMethod == "GET" {
		username := strings.Trim(strings.Split(r.Path, "/")[2], "\r\n ")
		user := GetUser(username)
		if user.Username != username {
			log.Println("could not find user ", username)
			return sdk.ShortRes(http.StatusNotFound, headers, "ain't here boss")
		}

		uString, err := json.Marshal(user)
		if err != nil {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}
		return sdk.ShortRes(http.StatusOK, headers, string(uString))
	} else {
		return sdk.NoAuthRes(http.StatusMethodNotAllowed, "nah, bro")
	}
}

func GetUser(username string) model.User {
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"username": {
				S: aws.String(username),
			},
			"role": {
				N: aws.String("1"),
			},
		},
	})
	if err != nil {
		log.Println("error calling GetItem", err)
		return model.User{}
	}

	user := model.User{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &user)
	if err != nil {
		log.Println("could not unmarshal user", err)
		return model.User{}
	}

	return user
}

func main() {
	lambda.Start(HandleRequest)
}
