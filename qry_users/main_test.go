package main

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"net/http"
	"testing"
)

var er = errors.New("test error")

func TestHandleRequest(t *testing.T) {
	//Fail - 405
	res, err := HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusMethodNotAllowed, res.StatusCode)
	assert.Equal(t, "nah, bro", res.Body)

	//Fail - no token
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusForbidden, res.StatusCode)
	assert.Equal(t, "invalid sender", res.Body)

	//Fail - could not find user
	svc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/Testie",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusNotFound, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "ain't here boss", res.Body)

	//Success
	svc = &mockOutItem{Item: map[string]*dynamodb.AttributeValue{
		"Username": {S: aws.String("Testie")},
		"Password": {S: aws.String("drowssap")},
		"Role":     {N: aws.String("1")},
		"Party":    {S: aws.String("bus")},
	},
		Err: nil,
	}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/users/Testie",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "{\"username\":\"Testie\",\"password\":\"drowssap\",\"role\":1,\"party\":\"bus\"}", res.Body)
}

func TestGetUser(t *testing.T) {
	//Success
	svc = &mockOutItem{Item: map[string]*dynamodb.AttributeValue{
		"Username": {S: aws.String("Testie")},
		"Password": {S: aws.String("drowssap")},
		"Role":     {N: aws.String("1")},
		"Party":    {S: aws.String("bus")},
	},
		Err: nil,
	}
	user := GetUser("Testie")
	assert.Equal(t, "Testie", user.Username)
	assert.Equal(t, "drowssap", user.Password)
	assert.Equal(t, model.Player, user.Role)
	assert.Equal(t, "bus", user.Party)

	//Fail - could not find user
	svc = &mockOutItem{Err: er}
	user = GetUser("nobody")
	assert.Equal(t, "", user.Username)
	assert.Equal(t, "", user.Password)
	assert.Equal(t, model.Stranger, user.Role)
	assert.Equal(t, "", user.Party)
}

type mockOutItem struct {
	dynamodbiface.DynamoDBAPI
	Item map[string]*dynamodb.AttributeValue
	Err  error
}

func (m mockOutItem) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {
	return &dynamodb.GetItemOutput{Item: m.Item}, m.Err
}
