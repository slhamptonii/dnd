package main

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

var er = errors.New("test error")

func TestHandleRequest(t *testing.T) {
	//Fail - 405
	res, err := HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusMethodNotAllowed, res.StatusCode)
	assert.Equal(t, "nah, bro", res.Body)

	//Fail - no token
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusForbidden, res.StatusCode)
	assert.Equal(t, "invalid sender", res.Body)

	//Fail - could not find party
	svc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/parties/bus",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusNotFound, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "ain't here boss", res.Body)

	//Fail - not a member
	svc = &mockOutItem{Item: map[string]*dynamodb.AttributeValue{
		"Name":     {S: aws.String("bus")},
		"Password": {S: aws.String("drowssap")},
		"DM":       {S: aws.String("chuck")},
		"Members":  {SS: []*string{aws.String("foo"), aws.String("bar")}},
	},
		Err: nil,
	}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path:                  "/parties/bus",
		QueryStringParameters: map[string]string{"username": "foobar"},
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusUnauthorized, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "not a party member", res.Body)

	//Success - dm
	svc = &mockOutItem{Item: map[string]*dynamodb.AttributeValue{
		"Id":       {S: aws.String("id")},
		"Name":     {S: aws.String("bus")},
		"Password": {S: aws.String("drowssap")},
		"DM":       {S: aws.String("chuck")},
		"Members":  {SS: []*string{aws.String("foo"), aws.String("bar")}},
		"Items":    nil,
	},
		Err: nil,
	}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path:                  "/parties/bus",
		QueryStringParameters: map[string]string{"username": "chuck"},
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "{\"id\":\"id\",\"name\":\"bus\",\"dm\":\"chuck\",\"members\":[\"foo\",\"bar\"],\"password\":\"drowssap\"}", res.Body)

	//Success - party member
	svc = &mockOutItem{Item: map[string]*dynamodb.AttributeValue{
		"Id":       {S: aws.String("id")},
		"Name":     {S: aws.String("bus")},
		"Password": {S: aws.String("drowssap")},
		"DM":       {S: aws.String("chuck")},
		"Members":  {SS: []*string{aws.String("foobar"), aws.String("bar")}},
		"Items":    nil,
	},
		Err: nil,
	}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path:                  "/parties/bus",
		QueryStringParameters: map[string]string{"dm": "chuck", "username": "foobar"},
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusOK, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "{\"id\":\"id\",\"name\":\"bus\",\"dm\":\"chuck\",\"members\":[\"foobar\",\"bar\"],\"password\":\"drowssap\"}", res.Body)
}

func TestGetUser(t *testing.T) {
	//Success
	svc = &mockOutItem{Item: map[string]*dynamodb.AttributeValue{
		"Name":     {S: aws.String("bus")},
		"Password": {S: aws.String("drowssap")},
		"DM":       {S: aws.String("chuck")},
		"Members":  {SS: []*string{aws.String("foo"), aws.String("bar")}},
	},
		Err: nil,
	}
	party := GetParty("bus", "chuck")
	assert.Equal(t, "bus", party.Name)
	assert.Equal(t, "drowssap", party.Password)
	assert.Equal(t, "chuck", party.DM)
	assert.Equal(t, []string{"foo", "bar"}, party.Members)

	//Fail - could not find party
	svc = &mockOutItem{Err: er}
	party = GetParty("bus", "chuck")
	assert.Equal(t, "", party.Name)
	assert.Equal(t, "", party.Password)
	assert.Equal(t, "", party.DM)
	assert.Equal(t, []string(nil), party.Members)
}

func TestEvaluateMember(t *testing.T) {
	assert.True(t, evaluateMember("foo", []string{"foo", "bar"}))
	assert.False(t, evaluateMember("foobar", []string{"foo", "bar"}))
}

type mockOutItem struct {
	dynamodbiface.DynamoDBAPI
	Item map[string]*dynamodb.AttributeValue
	Err  error
}

func (m mockOutItem) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {
	return &dynamodb.GetItemOutput{Item: m.Item}, m.Err
}
