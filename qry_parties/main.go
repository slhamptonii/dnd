package main

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	request "gitlab.com/slhamptonii/sheldonsandbox-core/v2/req"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
	"log"
	"net/http"
	"os"
)

var partyTable = os.Getenv("PARTY_TABLE")
var svc dynamodbiface.DynamoDBAPI

func init() {
	svc = sdk.DynamoConnect()
}

func HandleRequest(r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	ss := sdk.PullFromCtx(&r.RequestContext, "ss")[0]
	signedString, ok := ss.(string)
	if !ok {
		return sdk.NoAuthRes(http.StatusForbidden, "invalid sender")
	}
	headers := map[string]string{"x-ss": signedString}
	headers["content-type"] = "application/json"
	headers["access-control-allow-origin"] = "*"
	headers["access-control-expose-headers"] = "x-ss"
	headers["accept"] = "*/*"

	switch r.HTTPMethod {
	case "GET":
		name := request.ExtractPath(r.Path)[1]
		username := r.QueryStringParameters["username"]
		party := GetParty(name, username)

		if party.Name == "" {
			return sdk.ShortRes(http.StatusNotFound, headers, "ain't here boss")
		}

		if username != party.DM && !evaluateMember(username, party.Members) {
			return sdk.ShortRes(http.StatusUnauthorized, headers, "not a party member")
		}

		pString, err := json.Marshal(party)
		if err != nil {
			log.Println("could not marshal party", party.Name, err)
			return sdk.ShortRes(http.StatusInternalServerError, headers, "could not retrieve party")
		}

		return sdk.ShortRes(http.StatusOK, headers, string(pString))
	default:
		return sdk.NoAuthRes(http.StatusMethodNotAllowed, "nah, bro")
	}
}

func GetParty(name, dm string) model.Party {
	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(partyTable),
		Key: map[string]*dynamodb.AttributeValue{
			"name": {
				S: aws.String(name),
			},
			"dm": {
				S: aws.String(dm),
			},
		},
	})
	if err != nil {
		log.Println("error calling GetItem", err)
		return model.Party{}
	}

	party := model.Party{}
	err = dynamodbattribute.UnmarshalMap(result.Item, &party)
	if err != nil {
		log.Println("could not unmarshal party", err)
		return model.Party{}
	}

	return party
}

func evaluateMember(username string, members []string) bool {
	for _, member := range members {
		if member == username {
			return true
		}
	}

	return false
}

func main() {
	lambda.Start(HandleRequest)
}
