package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/stretchr/testify/assert"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"testing"
)

func TestHandleRequest(t *testing.T) {
	m := map[string]events.DynamoDBAttributeValue{
		"role":     events.NewNumberAttribute("1"),
		"username": events.NewStringAttribute("name"),
		"password": events.NewStringAttribute("word"),
		"party":    events.NewStringAttribute("bus"),
	}

	e := events.DynamoDBEvent{Records: []events.DynamoDBEventRecord{
		{
			Change: events.DynamoDBStreamRecord{
				NewImage: m,
			},
			EventName: "Foo",
		},
		{
			Change: events.DynamoDBStreamRecord{
				NewImage: m,
			},
			EventName:      "INSERT",
			EventSourceArn: "arn:aws:dynamodb:us-west-2:012345678910:table/sheldonsandbox_dev_cmd_foo/stream/2020-05-29T19:54:11.575",
		},
		{
			Change: events.DynamoDBStreamRecord{
				NewImage: m,
			},
			EventName:      "INSERT",
			EventSourceArn: "arn:aws:dynamodb:us-west-2:012345678910:table/sheldonsandbox_dev_cmd_user/stream/2020-05-29T19:54:11.575",
		},
	}}

	HandleRequest(nil, e)
}

func TestHandleEventName(t *testing.T) {
	assert.True(t, handleEventName("INSERT"))
	assert.True(t, handleEventName("REMOVE"))
	assert.True(t, handleEventName("MODIFY"))
	assert.False(t, handleEventName("foo"))
}

func TestDetermineQueueName(t *testing.T) {
	assert.Equal(t, "queue-cmd-user", determineQueueName("user"))
	assert.Equal(t, "queue-cmd-item", determineQueueName("item"))
	assert.Equal(t, "queue-cmd-party", determineQueueName("party"))
	assert.Equal(t, "queue-cmd-draft", determineQueueName("draft"))
	assert.Equal(t, "", determineQueueName("foo"))
}

func TestFindTable(t *testing.T) {
	arn := "arn:aws:dynamodb:us-west-2:012345678910:table/sheldonsandbox_dev_cmd_user/stream/2020-05-29T19:54:11.575"
	out := findTable(arn)
	assert.Equal(t, "user", out)
}

func TestMapRecordToUser(t *testing.T) {
	m := map[string]events.DynamoDBAttributeValue{
		"role":     events.NewNumberAttribute("1"),
		"username": events.NewStringAttribute("name"),
		"password": events.NewStringAttribute("word"),
		"party":    events.NewStringAttribute("bus"),
	}
	u := mapRecordToUser(m)

	assert.Equal(t, model.Player, u.Role)
	assert.Equal(t, "name", u.Username)
	assert.Equal(t, "bus", u.Party)
}

func TestMapRecordToMessage(t *testing.T) {
	m := map[string]events.DynamoDBAttributeValue{
		"role":     events.NewNumberAttribute("1"),
		"username": events.NewStringAttribute("name"),
		"password": events.NewStringAttribute("word"),
		"party":    events.NewStringAttribute("bus"),
	}

	record := events.DynamoDBEventRecord{
		Change: events.DynamoDBStreamRecord{
			NewImage: m,
		},
	}

	out := mapRecordToMessage("user", "INSERT", record)

	assert.Equal(t, "{\"username\":\"name\",\"password\":\"word\",\"role\":1,\"party\":\"bus\"}", out)
}

type mockSQSOutItem struct {
	sqsiface.SQSAPI
	Err error
}

func (m *mockSQSOutItem) GetQueueUrl(input *sqs.GetQueueUrlInput) (*sqs.GetQueueUrlOutput, error) {
	url := "url"
	return &sqs.GetQueueUrlOutput{QueueUrl: &url}, m.Err
}

func (m *mockSQSOutItem) SendMessage(input *sqs.SendMessageInput) (*sqs.SendMessageOutput, error) {
	return &sqs.SendMessageOutput{}, m.Err
}
