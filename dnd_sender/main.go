package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/google/uuid"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
	"log"
	"os"
	"strings"
)

var svc sqsiface.SQSAPI
var env = os.Getenv("ENVIRONMENT")
var cmdUserQueue = os.Getenv("QUEUE_CMD_USER")
var cmdItemQueue = os.Getenv("QUEUE_CMD_ITEM")
var cmdPartyQueue = os.Getenv("QUEUE_CMD_PARTY")
var cmdDraftQueue = os.Getenv("QUEUE_CMD_DRAFT")

func init() {
	svc = sdk.SQSConnect()
}

func HandleRequest(ctx context.Context, e events.DynamoDBEvent) {
	for _, record := range e.Records {
		log.Println(fmt.Sprintf("record info: %v", record))
		eventName := record.EventName

		if !handleEventName(eventName) {
			log.Println("skipping dynamo event", eventName)
			continue
		}

		table := findTable(record.EventSourceArn)
		body := mapRecordToMessage(table, eventName, record)
		qName := determineQueueName(table)
		if qName == "" {
			log.Println("could not determine queue name", table)
			continue
		}

		qUrl := sdk.GetQueueUrl(svc, &sqs.GetQueueUrlInput{
			QueueName: aws.String(qName),
		})
		if qUrl == "" {
			log.Println("could not get queue url", qName)
			continue
		}

		dedupeId := uuid.New().String()
		log.Println("sending", body)

		ok := sdk.SendMessage(svc, &sqs.SendMessageInput{
			DelaySeconds: aws.Int64(0),
			MessageAttributes: map[string]*sqs.MessageAttributeValue{
				"Environment": {
					DataType:    aws.String("String"),
					StringValue: aws.String(env),
				},
				"Table": {
					DataType:    aws.String("String"),
					StringValue: aws.String(table),
				},
				"Action": {
					DataType:    aws.String("String"),
					StringValue: aws.String(eventName),
				},
			},
			MessageBody:             aws.String(body),
			MessageDeduplicationId:  aws.String(dedupeId),
			MessageGroupId:          aws.String(table),
			MessageSystemAttributes: nil,
			QueueUrl:                aws.String(qUrl),
		})

		if !ok {
			msg := fmt.Sprintf("unable to send message. groupId: %v. deduplicationId: %v", eventName, dedupeId)
			log.Println(msg)
			continue
		}
	}
}

func handleEventName(name string) bool {
	log.Println("checking env", name)
	return name == "INSERT" || name == "REMOVE" || name == "MODIFY"
}

func determineQueueName(table string) string {
	switch table {
	case "user":
		return cmdUserQueue
	case "item":
		return cmdItemQueue
	case "party":
		return cmdPartyQueue
	case "draft":
		return cmdDraftQueue
	default:
		return ""
	}
}

func findTable(arn string) (out string) {
	//TODO: use regex
	fields := strings.Split(arn, "/")
	table := fields[1]
	segments := strings.Split(table, "_")
	out = segments[len(segments)-1]

	return out
}

func mapRecordToUser(m map[string]events.DynamoDBAttributeValue) model.User {
	u := model.User{}
	if r, ok := m["role"]; !ok {
		log.Println("missing role in record")
		return model.User{}
	} else {
		role, err := r.Integer()
		if err != nil {
			log.Println("could not convert user role", err)
			return model.User{}
		}
		u.Role = model.Role(role)
	}

	if username, ok := m["username"]; !ok {
		log.Println("missing username in record")
		return u
	} else {
		u.Username = username.String()
	}

	//Records to be removed will only have table keys present
	if password, ok := m["password"]; ok {
		u.Password = password.String()
	}

	if party, ok := m["party"]; ok {
		u.Party = party.String()
	}

	return u
}

func mapRecordToItem(m map[string]events.DynamoDBAttributeValue) model.Item {
	i := model.Item{}
	if id, ok := m["id"]; !ok {
		log.Println("missing id in record")
		return model.Item{}
	} else {
		i.Id = id.String()
	}

	if owner, ok := m["owner"]; !ok {
		log.Println("missing owner in record")
		return model.Item{}
	} else {
		i.Owner = owner.String()
	}

	if name, ok := m["name"]; ok {
		i.Name = name.String()
	}

	if desc, ok := m["description"]; ok {
		i.Description = desc.String()
	}

	return i
}

func mapRecordToParty(m map[string]events.DynamoDBAttributeValue) model.Party {
	p := model.Party{}

	if name, ok := m["name"]; !ok {
		log.Println("missing name in record")
		return model.Party{}
	} else {
		p.Name = name.String()
	}

	if dm, ok := m["dm"]; !ok {
		log.Println("missing dm in record")
		return model.Party{}
	} else {
		p.DM = dm.String()
	}

	if password, ok := m["password"]; ok {
		p.Password = password.String()
	}

	return p
}

func mapRecordToMessage(table, eventName string, record events.DynamoDBEventRecord) (out string) {
	log.Println("record change ", record.Change)
	log.Println("record change new image ", record.Change.NewImage)
	log.Println("record change old image ", record.Change.OldImage)
	log.Println("record change keys ", record.Change.Keys)
	switch table {
	case "user":
		var u model.User
		if eventName == "REMOVE" {
			u = mapRecordToUser(record.Change.Keys)
		} else {
			u = mapRecordToUser(record.Change.NewImage)
		}

		b, err := json.Marshal(u)
		if err != nil {
			log.Println("could not marshal user", err)
			return out
		}
		out = string(b)
	case "item":
		var i model.Item
		if eventName == "REMOVE" {
			i = mapRecordToItem(record.Change.Keys)
		} else {
			i = mapRecordToItem(record.Change.NewImage)
		}

		b, err := json.Marshal(i)
		if err != nil {
			log.Println("could not marshal user", err)
			return out
		}
		out = string(b)
	case "party":
		var p model.Party
		if eventName == "REMOVE" {
			p = mapRecordToParty(record.Change.Keys)
		} else {
			p = mapRecordToParty(record.Change.NewImage)
		}

		b, err := json.Marshal(p)
		if err != nil {
			log.Println("could not marshal user", err)
			return out
		}
		out = string(b)
	}

	return out
}

func main() {
	lambda.Start(HandleRequest)
}
