module gitlab.com/slhamptonii/dnd/cmd_parties

go 1.14

require (
	github.com/aws/aws-lambda-go v1.20.0
	github.com/aws/aws-sdk-go v1.36.0
	github.com/google/uuid v1.1.2
	github.com/stretchr/testify v1.6.1
	gitlab.com/slhamptonii/sheldonsandbox-core/v2 v2.7.0
	golang.org/x/crypto v0.0.0-20201124201722-c8d3bf9c5392 // indirect
)
