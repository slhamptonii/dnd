package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/aws/aws-lambda-go/events"
	lam "github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/aws/aws-sdk-go/service/lambda/lambdaiface"
	"github.com/google/uuid"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/crypt"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/model"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/req"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v2/sdk"
)

var tableName = os.Getenv("PARTY_TABLE")
var dynamoSvc dynamodbiface.DynamoDBAPI
var lambdaSvc lambdaiface.LambdaAPI

type PasswordChange struct {
	Name             string `json:"name"`
	DM               string `json:"dm"`
	OriginalPassword string `json:"originalPassword"`
	NewPassword      string `json:"newPassword"`
}

func init() {
	dynamoSvc = sdk.DynamoConnect()
	lambdaSvc = sdk.LambdaConnect()
}

func HandleRequest(r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	ss := sdk.PullFromCtx(&r.RequestContext, "ss")[0]
	signedString, ok := ss.(string)
	if !ok {
		return sdk.NoAuthRes(http.StatusForbidden, "invalid sender")
	}
	headers := map[string]string{"x-ss": signedString}
	headers["content-type"] = "application/json"
	headers["access-control-allow-origin"] = "*"
	headers["access-control-expose-headers"] = "x-ss"
	headers["accept"] = "*/*"

	switch r.HTTPMethod {
	case "POST":
		var party model.Party
		err := json.Unmarshal([]byte(r.Body), &party)
		if err != nil {
			log.Println(r.RequestContext.RequestID, "could not unmarshal payload", err)
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		if party.DM == "" {
			return sdk.ShortRes(http.StatusBadRequest, headers, "must have dungeon master")
		}

		users, found := checkUsers(party.Members, signedString)
		party.Members = users["valid"]

		foundDm, ok := checkUser(party.DM, signedString)
		if !ok {
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid dungeon master")
		}

		party.Password = crypt.HashAndSaltDefault([]byte(party.Password))
		party.Id = newId()

		ok = sdk.Put(dynamoSvc, tableName, party)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		found = append(found, foundDm)

		if ok = updateMembersAndDM(party.Name, found); !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "could not update associate folk")
		}

		party.Password = ""
		b, err := json.Marshal(party)
		if err != nil {
			log.Println("could not marshal party", err)
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		return sdk.ShortRes(http.StatusCreated, headers, string(b))
	case "PATCH":
		vars := request.ExtractPath(r.Path)
		name := vars[1]
		dm := r.QueryStringParameters["dm"]
		oldParty, ok := findParty(name, dm, signedString)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "unable to process party update")
		}

		var newParty model.Party
		err := json.Unmarshal([]byte(r.Body), &newParty)
		if err != nil {
			log.Println(r.RequestContext.RequestID, "could not unmarshal payload", err)
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		expressionAttributeNames := map[string]*string{}
		expressionAttributeValues := map[string]*dynamodb.AttributeValue{}
		key := map[string]*dynamodb.AttributeValue{}
		key["name"] = &dynamodb.AttributeValue{S: aws.String(oldParty.Name)}
		key["dm"] = &dynamodb.AttributeValue{S: aws.String(oldParty.DM)}

		nameExpression := "#NAME_EXPRESSION = :nameExpression"
		expressionAttributeNames["#NAME_EXPRESSION"] = aws.String("name")
		expressionAttributeValues[":nameExpression"] = &dynamodb.AttributeValue{S: aws.String(newParty.Name)}

		dmExpression := "#DM_EXPRESSION = :dmExpression"
		expressionAttributeNames["#DM_EXPRESSION"] = aws.String("dm")
		expressionAttributeValues[":dmExpression"] = &dynamodb.AttributeValue{S: aws.String(newParty.DM)}

		membersExpression := "#MEMBERS_EXPRESSION = :membersExpression"
		expressionAttributeNames["#MEMBERS_EXPRESSION"] = aws.String("members")
		expressionAttributeValues[":membersExpression"] = &dynamodb.AttributeValue{SS: aws.StringSlice(newParty.Members)}

		updateExpression := fmt.Sprintf("SET %s, %s, %s", nameExpression, dmExpression, membersExpression)

		updateInput := &dynamodb.UpdateItemInput{
			ExpressionAttributeNames:  expressionAttributeNames,
			ExpressionAttributeValues: expressionAttributeValues,
			Key:                       key,
			ReturnValues:              aws.String("ALL_NEW"),
			TableName:                 aws.String(tableName),
			UpdateExpression:          aws.String(updateExpression),
		}

		_, ok = sdk.UpdateItem(dynamoSvc, updateInput)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "unable to process user update")
		}

		b, err := json.Marshal(newParty)
		if err != nil {
			log.Println("could not marshal party", err)
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		return sdk.ShortRes(http.StatusOK, headers, string(b))
	case "PUT":
		vars := request.ExtractPath(r.Path)
		name := vars[1]

		dm := r.QueryStringParameters["dm"]
		oldParty, ok := findParty(name, dm, signedString)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "unable to process party update")
		}

		var passwordChange PasswordChange
		err := json.Unmarshal([]byte(r.Body), &passwordChange)
		if err != nil {
			log.Println("could not unmarshal payload", err.Error())
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid payload")
		}

		if !crypt.ComparePasswords(oldParty.Password, []byte(passwordChange.OriginalPassword)) {
			log.Println("invalid original password")
			return sdk.ShortRes(http.StatusForbidden, headers, "invalid old password")
		}

		expressionAttributeNames := map[string]*string{}
		expressionAttributeValues := map[string]*dynamodb.AttributeValue{}
		key := map[string]*dynamodb.AttributeValue{}
		key["name"] = &dynamodb.AttributeValue{S: aws.String(passwordChange.Name)}
		key["dm"] = &dynamodb.AttributeValue{S: aws.String(passwordChange.DM)}

		passwordExpression := "#PASSWORD_EXPRESSION = :passwordExpression"
		expressionAttributeNames["#PASSWORD_EXPRESSION"] = aws.String("password")
		expressionAttributeValues[":passwordExpression"] = &dynamodb.AttributeValue{S: aws.String(crypt.HashAndSaltDefault([]byte(passwordChange.NewPassword)))}

		idExpression := "#ID_EXPRESSION = :idExpression"
		expressionAttributeNames["#ID_EXPRESSION"] = aws.String("id")
		expressionAttributeValues[":idExpression"] = &dynamodb.AttributeValue{S: aws.String(newId())}

		updateExpression := fmt.Sprintf("SET %s, %s", passwordExpression, idExpression)

		updateInput := &dynamodb.UpdateItemInput{
			ExpressionAttributeNames:  expressionAttributeNames,
			ExpressionAttributeValues: expressionAttributeValues,
			Key:                       key,
			ReturnValues:              aws.String("ALL_NEW"),
			TableName:                 aws.String(tableName),
			UpdateExpression:          aws.String(updateExpression),
		}

		_, ok = sdk.UpdateItem(dynamoSvc, updateInput)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		passwordChange.OriginalPassword = ""
		passwordChange.NewPassword = ""
		b, err := json.Marshal(passwordChange)
		if err != nil {
			log.Println("could not marshal party", err)
			return sdk.ShortRes(http.StatusInternalServerError, headers, "we have failed you")
		}

		return sdk.ShortRes(http.StatusOK, headers, string(b))
	case "DELETE":
		dm := r.QueryStringParameters["dm"]
		if dm == "" {
			return sdk.ShortRes(http.StatusBadRequest, headers, "invalid dm")
		}

		name := request.ExtractPath(r.Path)[1]

		ok := DeleteParty(name, dm)
		if !ok {
			return sdk.ShortRes(http.StatusInternalServerError, headers, "could not delete party")
		}
		return sdk.ShortRes(http.StatusNoContent, headers, "")
	default:
		return sdk.NoAuthRes(http.StatusMethodNotAllowed, "nah, bro")
	}
}

func DeleteParty(name, dm string) bool {
	input := &dynamodb.DeleteItemInput{
		Key: map[string]*dynamodb.AttributeValue{
			"name": {
				S: aws.String(name),
			},
			"dm": {
				S: aws.String(dm),
			},
		},
		TableName: aws.String(tableName),
	}

	return sdk.DeleteItem(dynamoSvc, input)
}

func checkUsers(users []string, token string) (map[string][]string, []model.User) {
	out := map[string][]string{"valid": {}, "invalid": {}}
	found := make([]model.User, 0)

	for _, user := range users {
		if u, ok := checkUser(user, token); ok {
			out["valid"] = append(out["valid"], user)
			found = append(found, u)
		} else {
			out["invalid"] = append(out["invalid"], user)
		}
	}

	log.Println("invalid users", out["invalid"])

	return out, found
}

func checkUser(username, token string) (model.User, bool) {
	path := fmt.Sprintf("/users/%s", username)
	req := events.APIGatewayProxyRequest{
		Path:       path,
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{
			Identity:   events.APIGatewayRequestIdentity{},
			Authorizer: map[string]interface{}{"ss": token},
		},
	}

	payload, err := json.Marshal(req)
	if err != nil {
		log.Println("could not marshal lambda request ", err)
		return model.User{}, false
	}

	//name := fmt.Sprintf("sheldonsandbox-qry-users", strings.ToLower(env))

	input := &lambda.InvokeInput{
		FunctionName: aws.String("sheldonsandbox-qry-users"),
		Payload:      payload,
	}

	if output, ok := sdk.Invoke(lambdaSvc, input); !ok {
		log.Println("function invoke failed ", *output.FunctionError)
		return model.User{}, ok
	} else {
		var user model.User
		var res events.APIGatewayProxyResponse
		r := bytes.NewReader(output.Payload)
		err = json.NewDecoder(r).Decode(&res)
		if err != nil {
			log.Println("could not decode response", err)
			return model.User{}, false
		}

		err := json.Unmarshal([]byte(res.Body), &user)
		if err != nil {
			log.Println("could not unmarshal body into user ", res.Body, err)
			return model.User{}, false
		}
		if user.Role == model.Stranger {
			log.Println("user not found ", username)
			log.Println("output ", string(output.Payload))
			log.Println("status", *output.StatusCode)
			return model.User{}, false
		}

		if user.Username == username {
			return user, true
		} else {
			return model.User{}, false
		}
	}
}

func findParty(name, dm, signedString string) (model.Party, bool) {
	var party model.Party
	log.Println("searching for", name)
	path := fmt.Sprintf("/parties/%s", name)
	req := events.APIGatewayProxyRequest{
		Path:       path,
		HTTPMethod: "GET",
		RequestContext: events.APIGatewayProxyRequestContext{
			Identity:   events.APIGatewayRequestIdentity{},
			Authorizer: map[string]interface{}{"ss": signedString},
		},
		QueryStringParameters: map[string]string{"username": dm},
	}

	payload, err := json.Marshal(req)
	if err != nil {
		return party, false
	}

	invokeInput := &lambda.InvokeInput{
		FunctionName: aws.String("sheldonsandbox-qry-parties"),
		Payload:      payload,
	}

	if output, ok := sdk.Invoke(lambdaSvc, invokeInput); !ok {
		return party, false
	} else {
		var res events.APIGatewayProxyResponse
		err = json.Unmarshal(output.Payload, &res)
		if err != nil {
			log.Println("could not unmarshal response", err.Error())
			return party, false
		}

		err = json.Unmarshal([]byte(res.Body), &party)
		if err != nil {
			log.Println("could not unmarshal body into party", res.Body, err.Error())
			return party, false
		}
	}

	return party, true
}

func updateMembersAndDM(partyName string, found []model.User) bool {
	for _, user := range found {
		user.Party = partyName
		if !PutUser(user) {
			return false
		}
	}

	return true
}

func PutUser(user model.User) bool {
	av, err := dynamodbattribute.MarshalMap(user)
	if err != nil {
		log.Println("could not marshal user", err)
		return false
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(tableName),
	}

	return sdk.PutItem(dynamoSvc, input)
}

func newId() string {
	return uuid.New().String()
}

func main() {
	lam.Start(HandleRequest)
}
