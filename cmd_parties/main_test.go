package main

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

var er = errors.New("test error")

func TestHandleRequest(t *testing.T) {
	//405
	res, err := HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "HEAD",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusMethodNotAllowed, res.StatusCode)
	assert.Equal(t, "nah, bro", res.Body)

	//Fail - No Token
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "DELETE",
	})
	assert.Nil(t, err)
	assert.Nil(t, res.Headers)
	assert.Equal(t, http.StatusForbidden, res.StatusCode)
	assert.Equal(t, "invalid sender", res.Body)

	//Delete Fail - missing query params
	dynamoSvc = &mockOutItem{}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "DELETE",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/parties/bus",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "invalid dm", res.Body)

	//Delete Fail - could not delete
	dynamoSvc = &mockOutItem{Err: er}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "DELETE",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path:                  "/parties/bus",
		QueryStringParameters: map[string]string{"dm": "chuck"},
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusInternalServerError, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "could not delete party", res.Body)

	//Delete Success
	dynamoSvc = &mockOutItem{}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "DELETE",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path:                  "/parties/bus",
		QueryStringParameters: map[string]string{"dm": "chuck"},
	})
	assert.Equal(t, http.StatusNoContent, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "", res.Body)
	assert.Nil(t, err)

	//Create Fail - bad json
	dynamoSvc = &mockOutItem{}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/parties",
		Body: "!@#$%^&*()",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "invalid payload", res.Body)

	//Create Fail - no dm
	dynamoSvc = &mockOutItem{}
	res, err = HandleRequest(events.APIGatewayProxyRequest{
		HTTPMethod: "POST",
		RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
			"ss": "a.to.ken",
		}},
		Path: "/parties",
		Body: "{\"name\": \"bus\"}",
	})
	assert.Nil(t, err)
	assert.Equal(t, http.StatusBadRequest, res.StatusCode)
	assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	assert.Equal(t, "must have dungeon master", res.Body)

	//Create Fail - invalid dm
	//dynamoSvc = &mockOutItem{}
	//res, err = HandleRequest(events.APIGatewayProxyRequest{
	//	HTTPMethod: "POST",
	//	RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
	//		"ss": "a.to.ken",
	//	}},
	//	Path: "/parties",
	//	Body: "{\"name\": \"bus\", \"dm\":\"bar\"}",
	//})
	//assert.Nil(t, err)
	//assert.Equal(t, http.StatusBadRequest, res.StatusCode)
	//assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	//assert.Equal(t, "invalid dungeon master", res.Body)
	//
	////Create Fail - bad dynamo put
	//dynamoSvc = &mockOutItem{Err: er}
	//res, err = HandleRequest(events.APIGatewayProxyRequest{
	//	HTTPMethod: "POST",
	//	RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
	//		"ss": "a.to.ken",
	//	}},
	//	Path: "/users/someone/items/chicken",
	//	Body: "{\"name\": \"bus\", \"dm\":\"foo\"}",
	//})
	//assert.Nil(t, err)
	//assert.Equal(t, http.StatusInternalServerError, res.StatusCode)
	//assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	//assert.Equal(t, "we have failed you", res.Body)
	//
	////Create Success
	//dynamoSvc = &mockOutItem{}
	//res, err = HandleRequest(events.APIGatewayProxyRequest{
	//	HTTPMethod: "POST",
	//	RequestContext: events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
	//		"ss": "a.to.ken",
	//	}},
	//	Path: "/users/someone",
	//	Body: "{\"name\": \"bus\", \"dm\":\"foo\"}",
	//})
	//assert.Nil(t, err)
	//assert.Equal(t, http.StatusCreated, res.StatusCode)
	//assert.Equal(t, "a.to.ken", res.Headers["x-ss"])
	//assert.Equal(t, "{\"name\":\"bus\",\"password\":\"\",\"dm\":\"foo\",\"members\":[],\"items\":null}", res.Body)
}

func TestDeleteParty(t *testing.T) {
	//Success
	dynamoSvc = &mockOutItem{}
	ok := DeleteParty("name", "dm")
	assert.Equal(t, true, ok)

	//Fail
	dynamoSvc = &mockOutItem{Err: er}
	ok = DeleteParty("name", "dm")
	assert.Equal(t, false, ok)
}

//func TestCheckUsers(t *testing.T) {
//	users := []string{"foo", "bar", "foobar"}
//	res := checkUsers(users, "foo")
//	assert.Equal(t, 2, len(res))
//	assert.Equal(t, 1, len(res["valid"]))
//	assert.Equal(t, 2, len(res["invalid"]))
//	assert.Equal(t, "foo", res["valid"][0])
//	assert.Equal(t, "bar", res["invalid"][0])
//	assert.Equal(t, "foobar", res["invalid"][1])
//}

//func TestCheckUser(t *testing.T) {
//	//Fail - request error
//	ok := checkUser("foo", "token")
//	assert.False(t, ok)
//
//	//Fail - response decode
//	ok = checkUser("foo", "token")
//	assert.False(t, ok)
//
//	//Success
//	ok = checkUser("foo", "token")
//	assert.True(t, ok)
//}

type mockOutItem struct {
	dynamodbiface.DynamoDBAPI
	Err error
}

func (m *mockOutItem) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	return &dynamodb.PutItemOutput{}, m.Err
}

func (m mockOutItem) DeleteItem(input *dynamodb.DeleteItemInput) (*dynamodb.DeleteItemOutput, error) {
	return &dynamodb.DeleteItemOutput{}, m.Err
}
